import java.io.File;
import java.util.Calendar;

import org.jsoup.nodes.Document;

public class Assignment {

	private Calendar dueDate;
	private Calendar assignDate;
	private String title;
	private Document doc;
	private File xmlFile;
	
	public Assignment(Calendar dueDate, Calendar assignDate, String title, Document doc, File xmlFile) {
		this.dueDate = dueDate;
		this.assignDate = assignDate;
		this.title = title;
		this.doc = doc;
		this.xmlFile = xmlFile;
	}

	public Calendar getDueDate() {
		return dueDate;
	}

	public void setDueDate(Calendar dueDate) {
		this.dueDate = dueDate;
	}

	public Calendar getAssignDate() {
		return assignDate;
	}

	public void setAssignDate(Calendar assignDate) {
		this.assignDate = assignDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}

	public File getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}

}
