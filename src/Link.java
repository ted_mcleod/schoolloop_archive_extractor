// <link ID="1443779452196" Title="Worksheet 10.2" NounID="1442646129706" URL="/file/1438443836783/1407475265587/3285133142738714437.doc"/>

//<link ID="1445756329232" Title="Lab 10.1 Problem 3 Shorthand" NounID="1445668294695" URL="http://www.mrferrante.com/apcs/WebLessons/LessonA10/Lab-A10-1.html"/>
public class Link {
	private String title;
	private String url;
	private boolean isFile;
	
	public Link(String title, String url) {
		this.title = title;
		isFile = url.startsWith("/file/");
		if (isFile) {
			url = url.substring(url.lastIndexOf('/') + 1);
		}
		this.url = url;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isFile() {
		return isFile;
	}

	public void setFile(boolean isFile) {
		this.isFile = isFile;
	}

	@Override
	public String toString() {
		return "<a href=" + url + (isFile ? " download" : "") + ">" + title + "</a>";
	}
	
}
