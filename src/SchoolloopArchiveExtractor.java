import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JFileChooser;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SchoolloopArchiveExtractor {

	public static void main(String[] args) {

		ConcurrentHashMap<String, File> dirMap = new ConcurrentHashMap<>();
		try {
			EventQueue.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					File inputDirectory = askDirectory("Choose a folder to extract files from", null);
					if (inputDirectory == null) {
						System.out.println("No extraction folder selected. Extraction cancelled");
						abort();
						return;
					}
					dirMap.put("inputDirectory", inputDirectory);
				}
			});
		} catch (InvocationTargetException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			EventQueue.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					File outputDirectory = askDirectory("Choose a folder to save extracted files to", dirMap.get("inputDirectory"));
					if (outputDirectory == null) {
						System.out.println("No output folder selected. Extraction cancelled");
						abort();
						return;
					}
					dirMap.put("outputDirectory", outputDirectory);
				}

			});
		} catch (InvocationTargetException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			extractAssignmentXMLFiles(dirMap.get("inputDirectory"), dirMap.get("outputDirectory"));
			createHTMLFilesFromXMLFiles(dirMap.get("outputDirectory"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Assignment getAssignmentFromXML(File xmlFile) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		DocumentBuilderFactory factory = getDocBUilderFactory();
		org.w3c.dom.Element xml = parseXML(factory, xmlFile.getAbsolutePath());

		org.w3c.dom.Node assignmentNode = xml.getElementsByTagName("assignment").item(0);

		NamedNodeMap assignAttr = assignmentNode.getAttributes();

		String title = getContentOfNamedNode(assignAttr, "Title");
		String description = getContentOfNamedNode(assignAttr, "Description");
		String dueDayID = getContentOfNamedNode(assignAttr, "DueDayID");
		String timeEstimate = getContentOfNamedNode(assignAttr, "TimeEstimate");
		String maxPoints = getContentOfNamedNode(assignAttr, "MaxPoints");
		String createdDayID = getContentOfNamedNode(assignAttr, "CreatedDayID");

		Calendar dueDate = new Calendar.Builder().setInstant(Long.parseLong(dueDayID)).build();
		Calendar assignDate = new Calendar.Builder().setInstant(Long.parseLong(createdDayID)).build();

		org.w3c.dom.Node linksNode = getFirstChildNodeByName(assignmentNode, "links");
		List<Link> links = getLinksInNode(linksNode);

		// create the html
		Document doc = Jsoup.parse("");

		Element html = doc.createElement("html");

		Element head = doc.createElement("head");
		html.attr("lang", "en");

		Element pageTitle = doc.createElement("title");
		pageTitle.text(title);

		head.appendChild(pageTitle);
		html.appendChild(head);

		Element body = doc.createElement("body");

		Element assignTitle = doc.createElement("h1");
		assignTitle.text(title);
		body.appendChild(assignTitle);

		Element assignDateP = doc.createElement("p");
		assignDateP.text(String.format("Assigned: %s", getDateString(createdDayID)));
		body.appendChild(assignDateP);

		Element dueDateP = doc.createElement("p");
		dueDateP.text(String.format("Due: %s", getDateString(dueDayID)));
		body.appendChild(dueDateP);

		Element timeEstimateP = doc.createElement("p");
		timeEstimateP.text(String.format("Time Estimate: %s", timeEstimate));
		body.appendChild(timeEstimateP);

		Element maxPointsP = doc.createElement("p");
		maxPointsP.text(String.format("Max Points: %s", maxPoints));
		body.appendChild(maxPointsP);

		Element descripTitle = doc.createElement("h3");
		descripTitle.text("Description");
		body.appendChild(descripTitle);

		Element descripP = doc.createElement("div");
		descripP.append(Entities.unescape(description));
		body.appendChild(descripP);

		if (links.size() > 0) {
			Element linksTitle = doc.createElement("h3");
			linksTitle.text("Links");
			body.appendChild(linksTitle);

			Element linksList = doc.createElement("ul");
			for (Link link : links) {
				Element linkLi = doc.createElement("li");
				linkLi.append(link.toString());
				linksList.appendChild(linkLi);
			}

			body.appendChild(linksList);
		}

		html.appendChild(body);
		doc.appendChild(html);
		Assignment assignment = new Assignment(dueDate, assignDate, title, doc, xmlFile);
		return assignment;
	}

	private static String getContentOfNamedNode(NamedNodeMap map, String nodeName) {
		org.w3c.dom.Node n = map.getNamedItem(nodeName);
		if (n == null) return "";
		return n.getTextContent();
	}

	private static String getDateString(String msSinceEpoch) {
		Calendar cal = new Calendar.Builder().setInstant(Long.parseLong(msSinceEpoch)).build();
		return new SimpleDateFormat("MM-dd-yyyy").format(cal.getTime());
	}

	// The links will assume the files are in the same directory as the html
	private static List<Link> getLinksInNode(org.w3c.dom.Node linksNode) {
		ArrayList<Link> links = new ArrayList<>();

		if (linksNode != null) {
			NodeList children = linksNode.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {
				org.w3c.dom.Node child = children.item(i);
				if (child.getNodeName().equals("link")) {
					NamedNodeMap attributes = child.getAttributes();
					String title = attributes.getNamedItem("Title").getTextContent();
					String url = attributes.getNamedItem("URL").getTextContent();
					Link link = new Link(title, url);
					links.add(link);
				}
			}
		}
		return links;
	}

	private static org.w3c.dom.Node getFirstChildNodeByName(org.w3c.dom.Node parent, String childName) {
		NodeList children = parent.getChildNodes();

		for (int i = 0; i < children.getLength(); i++) {
			org.w3c.dom.Node child = children.item(i);
			if (child.getNodeName().equals(childName)) return child;
		}
		return null;
	}

	private static org.w3c.dom.Element parseXML(DocumentBuilderFactory factory, String filePath) throws SAXException, IOException, ParserConfigurationException {
		org.w3c.dom.Document input = factory.newDocumentBuilder().parse(filePath);
		org.w3c.dom.Element xml = input.getDocumentElement();
		return xml;
	}

	private static DocumentBuilderFactory getDocBUilderFactory() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		return factory;
	}

	public static void createHTMLFilesFromXMLFiles(File outputDirectory) {

		Document doc = Jsoup.parse("");
		Element html = doc.createElement("html");
		Element head = doc.createElement("head");
		html.attr("lang", "en");
		Element title = doc.createElement("title");
		title.text(outputDirectory.getName());
		head.appendChild(title);
		html.appendChild(head);
		Element body = doc.createElement("body");
		Element courseTitle = doc.createElement("h1");
		courseTitle.text(outputDirectory.getName());
		body.appendChild(courseTitle);
		Element table = doc.createElement("table");
		table.append("<tr><th>Assign Date</th><th>Due Date</th><th>Assignment</th></tr>");
		body.appendChild(table);
		html.appendChild(body);
		doc.appendChild(html);

		List<Assignment> assignments = getAssignmentsFromXmlFiles(outputDirectory);
		assignments.sort(new Comparator<Assignment>() {

			@Override
			public int compare(Assignment a, Assignment b) {
				return a.getAssignDate().compareTo(b.getAssignDate());
			}

		});

		for (Assignment assignment : assignments) {
			File file = assignment.getXmlFile();
			String fName = file.getName();
			String saveName = FileUtil.getNameWithoutExtension(fName) + ".html";
			File saveFile = new File(outputDirectory.getAbsolutePath() + "/" + saveName);
			Document assignDoc = assignment.getDoc();
			String assignDate = new SimpleDateFormat("MM-dd-yyyy").format(assignment.getAssignDate().getTime());
			String dueDate = new SimpleDateFormat("MM-dd-yyyy").format(assignment.getDueDate().getTime());
			table.append("<tr><td>" + assignDate + "</td><td>" + dueDate + "</td><td><a href=" + saveName + ">" + assignment.getTitle() + "</a></td></tr>");
			System.out.println("Writing " + saveFile.getAbsolutePath());
			FileUtil.writeString(saveFile, assignDoc.toString());
		}

		File indexFile = new File(outputDirectory.getAbsolutePath() + "/index.html");
		System.out.println("Writing " + indexFile.getAbsolutePath());
		FileUtil.writeString(indexFile, doc.toString());
	}

	private static List<Assignment> getAssignmentsFromXmlFiles(File dir) {
		ArrayList<Assignment> assignments = new ArrayList<>();
		File[] files = dir.listFiles();
		for (File file : files) {
			String fName = file.getName();
			String ext = getExtension(fName);
			if (ext.equalsIgnoreCase("xml") && fName.startsWith("SL__assignment__")) {
				try {
					assignments.add(getAssignmentFromXML(file));
				} catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
					e.printStackTrace();
				}
			}
		}
		return assignments;
	}

	private static void extractAssignmentXMLFiles(File inputDirectory, File outputDirectory) throws IOException {
		File[] files = inputDirectory.listFiles();
		for (File file : files) {
			String fName = file.getName();
			String ext = getExtension(fName);
			if (ext.equalsIgnoreCase("gz") && fName.startsWith("SL__assignment__")) {
				String saveName = fName.substring(0, fName.length() - ext.length() - 1);
				File saveFile = new File(outputDirectory.getAbsolutePath() + "/" + saveName);
				System.out.println("Extracting " + file.getAbsolutePath() + " to " + saveFile.getAbsolutePath());
				try {
					UnzipUtility.extract(file.getAbsolutePath(), saveFile.getAbsolutePath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				File targetFile = new File(outputDirectory.getAbsolutePath() + "/" + fName);
				Files.copy(file.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
		}
	}

	private static String getExtension(String fileName) {
		int lastPer = fileName.lastIndexOf('.');
		if (lastPer > -1) return fileName.substring(lastPer + 1);
		return "";
	}

	private static void abort() {
		System.out.println("Aborted.");
		System.exit(0);
	}

	private static File askDirectory(String title, File startDir) {
		JFileChooser fc = new JFileChooser(startDir);
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.setDialogTitle(title);
		int state = fc.showOpenDialog(null);
		File file = null;
		if (state == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
		}
		return file;
	}

}
