
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.nio.charset.Charset;


public class FileUtil {
	public static final PrintStream SYS_OUT = System.out;
	public static final String UTF8 = java.nio.charset.StandardCharsets.UTF_8.toString();
	public static final Charset UTF_8 = java.nio.charset.StandardCharsets.UTF_8;
	
	public static String read(File file) throws FileNotFoundException, IOException {
		InputStreamReader reader = null;
		String str = "";
		reader = new InputStreamReader(new FileInputStream(file), UTF_8);
		while (reader.ready()) {
			str += (char)reader.read();
		}
		reader.close();
		return str;
	}
	
	public static void writeString(String path, String str) {
		writeString(new File(path), str);
	}
	
	public static void writeString(File file, String str) {
		try {
			//SYS_OUT.println("writing to file: " + file.getAbsolutePath());
			if (file.getParentFile() != null) {
				file.getParentFile().mkdirs();
			}
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), UTF_8);
			for (int i = 0; i < str.length(); i++) {
				char c = str.charAt(i);
				writer.write(c);
			}
			writer.close();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public static String getExtension(String fileName) {
		int start = fileName.lastIndexOf('.');
		if (start >= 0) {
			return fileName.substring(start);
		}
		return "";
	}
	
	public static String getExtension(File file) {
		return getExtension(file.getName());
	}
	
	public static String getNameWithoutExtension(String fileName) {
		return fileName.substring(0, fileName.length() - getExtension(fileName).length());
	}
	
	public static String getNameWithoutExtension(File file) {
		return getNameWithoutExtension(file.getName());
	}
	
	public static String fileSafeName(String s) {
		return s.replaceAll("\\s", "+").replaceAll("[^+a-zA-Z0-9.-]", "");
	}
}
