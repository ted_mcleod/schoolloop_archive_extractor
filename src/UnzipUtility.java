import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.GZIPInputStream;
 
/**
 * This utility extracts files and directories of a standard zip file to
 * a destination directory.
 * @author www.codejava.net
 *
 */
public class UnzipUtility {
    /**
     * Size of the buffer to read/write data
     */
    private static final int BUFFER_SIZE = 4096;
    /**
     * Extracts a zip file specified by the zipFilePath to a directory specified by
     * destDirectory (will be created if does not exists)
     * @param zipFilePath
     * @param destDirectory
     * @throws IOException
     */
    public static void extract(String filePath, String toFilePath) throws IOException {
        if (filePath.toLowerCase().endsWith(".zip")) {
        	// a zip file always extracts to a directory which contains the contents of the zip file
        	File destDir = new File(toFilePath);
        	if (!destDir.exists()) {
                destDir.mkdir();
            }
        	unZip(filePath, toFilePath);
        } else if (filePath.toLowerCase().endsWith(".gz")) {
        	gunzip(filePath, toFilePath);
        } else {
        	System.out.println(filePath + " is not a zip or gz file");
        }
        
    }
    
    public static void unZip(String zipFilePath, String destDirPath) throws IOException {
    	ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirPath + File.separator + entry.getName();
            new File(filePath).getParentFile().mkdirs();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractToFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }
    
    public static void gunzip(String gzFilePath, String toFilePath) throws IOException {
    	GZIPInputStream in = new GZIPInputStream(new FileInputStream(gzFilePath));
    	extractToFile(in, toFilePath);
    }
    
    /**
     * Extracts a zip entry (file entry)
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    private static void extractToFile(InputStream in, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = in.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
}
