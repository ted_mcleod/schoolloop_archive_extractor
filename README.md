# README #

### What is this program for? ###

This program will extract and parse the contents of a schoolloop course archive and create a webpage for browsing the assignments ordered by assigned date.

## Download the program ##

## Windows ##

Download [SLArchiveExtractor.exe](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/SLArchiveExtractor.exe)

## Any other platform such as Mac or Linux (pure Java) ##

Download [schoolloop_archive_extractor.jar](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/schoolloop_archive_extractor.jar)

## Trouble downloading? ##

You may be warned that the jar or exe file is dangerous because it is not a known application. You can tell your browser to keep the file anyways.

## Don't have Java installed? ##

If you don't have a Java runtime installed, you will need to [download and install](https://java.com/en/download/) it.

## Downloading the Course Archives ##

Go to schoolloop and click on the Course Archives link in your home screen.

![Course Archives Link](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/course_archives_link.png)

For each archived course, click on the Download Course Content link.

![Download Course Content](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/download_course_content.png)

Each downloaded course will consist of a zip file named according to the course and year.

## Extract Zip File Windows ##

Right click on each zip file and choose Extract All...
  
 ![Extract All](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/extract_all.png)
 ![Extract All Wizard](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/extract_all_wizard.png)

## Extract File Mac ##

On a mac you can just double click on the zip file to extract the contents.

## Run the Program on Windows ##

Double click on the program SLArchiveExtractor.exe that you downloaded earlier.

![Open SLArchiveExtractor.exe](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/open_slarchiveextractor.png)

You may get a warning from microsoft saying windows prevented an unrecognized app from starting.

* To get past the warning click on the More Info link and then press the Run Anyway button.
* You may also get warnings from virus protection software saying it might be dangerous because it does not recognize it as a regularly used application. You can generally tell it to run anyways and add the app to a white list of some sort depending on the virus protection software you are using.

![Unrecognized App Warning](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/unrecognized_app_warning.png)
![Unrecognized App Run Anyway](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/unrecognized_app_run_anyway.png)

## Run the Jar Program on Mac (Commandline method works for all platforms) ##

* Option 1: Right click on the *schoolloop_archive_extractor.jar* file you downloaded and select open with Jar Launcher (you may be asked to confirm you want to run it)
* Option 2: Open the Terminal app (or cmd prompt for windows users running the jar):
  * Navigate to the folder containing the *schoolloop_archive_extractor.jar* file you downloaded using this command:
        
		cd path/to/the/folder
		
  * Note: You can type cd and then drag the folder from the finder into the terminal and it will type the path to the folder for you.
  * Run the jar by typing this command:
        
		java -jar schoolloop_archive_extractor.jar

## Select the Source and Destination Folders ##

* When the program loads, it will pop up with a prompt asking you to choose a folder to extract files from:
  * Open the folder you extracted the zip file to (should be named according to your course and year (Example: *AP_Comp_Sci_A_2018_Archive*)
  * select the *archivedFiles* subfolder (that is the folder containing all the course content files)
  
![Select archivedFiles folder](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/select_archivedFiles.png)

You will then be prompted to choose a folder to save extracted files to:

You can choose any folder you wish and it will copy all the content and html files to that folder
  
## Expected Output ##

After running, the folder you selected to save extraced files to should contain all the content files, plus some html files.

To view all the assignments by assigned date, simply open the *index.html* file in a browser (double clicking should work)

![Open index.html](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/open_index.png)

You should see something like this:

![index.html sample](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/course_archive_index.html_sample.png)

Clicking on an assignment should take you to a page like this:

![Assignment Page Sample](https://bitbucket.org/ted_mcleod/schoolloop_archive_extractor/downloads/assignment_page_sample.png)


